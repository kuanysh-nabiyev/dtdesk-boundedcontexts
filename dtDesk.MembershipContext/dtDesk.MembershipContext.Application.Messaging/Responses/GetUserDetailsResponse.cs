﻿using dtDesk.MembershipContext.DomainModel.Users;

namespace dtDesk.MembershipContext.Application.Messaging.Responses
{
    public class GetUserDetailsResponse
    {
        public User User { get; set; }
    }
}