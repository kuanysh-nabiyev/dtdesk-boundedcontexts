using System.Collections.Generic;
using dtDesk.MembershipContext.DomainModel.Users;

namespace dtDesk.MembershipContext.Application.Messaging.Responses
{
    public class GetUsersResponse
    {
        public IEnumerable<User> Users { get; set; }
    }
}