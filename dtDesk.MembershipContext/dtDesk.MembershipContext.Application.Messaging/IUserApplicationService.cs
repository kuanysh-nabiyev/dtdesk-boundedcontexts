﻿using dtDesk.MembershipContext.Application.Messaging.Requests;
using dtDesk.MembershipContext.Application.Messaging.Responses;

namespace dtDesk.MembershipContext.Application.Messaging
{
    public interface IUserApplicationService
    {
        GetUserDetailsResponse GetUserDetails(GetUserDetailsRequest request);
        GetUsersResponse GetUsers(GetUsersRequest request = null);
    }
}
