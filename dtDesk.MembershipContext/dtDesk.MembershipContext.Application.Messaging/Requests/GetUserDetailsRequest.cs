namespace dtDesk.MembershipContext.Application.Messaging.Requests
{
    public class GetUserDetailsRequest
    {
        public int UserId { get; set; }
    }
}