﻿using System;
using System.Linq;
using System.Linq.Expressions;
using dtDesk.MembershipContext.DomainModel.DomainServices;
using dtDesk.MembershipContext.DomainModel.Repositories;
using dtDesk.MembershipContext.DomainModel.Users;

namespace dtDesk.MembershipContext.Domain.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public User GetUserDetails(int userId)
        {
            try
            {
                return _userRepository.Find(u => u.UserId == userId).Single();
            }
            catch (Exception ex)
            {
                // TODO: create new type of exception
                throw new Exception("exception:" + ex.Message);
            }
        }
    }
}