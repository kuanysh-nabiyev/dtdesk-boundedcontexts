﻿using dtDesk.MembershipContext.Application.Messaging;
using dtDesk.MembershipContext.Application.Messaging.Requests;
using dtDesk.MembershipContext.Application.Messaging.Responses;
using dtDesk.MembershipContext.DomainModel.DomainServices;
using dtDesk.MembershipContext.DomainModel.Repositories;

namespace dtDesk.MembershipContext.Application.Services
{
    public class UserApplicationService : IUserApplicationService
    {
        private readonly IUserService _userService;
        private readonly IUserRepository _userRepository;

        public UserApplicationService(IUserService userService, IUserRepository userRepository)
        {
            _userService = userService;
            _userRepository = userRepository;
        }

        public GetUserDetailsResponse GetUserDetails(GetUserDetailsRequest request)
        {
            return new GetUserDetailsResponse()
            {
                User = _userService.GetUserDetails(request.UserId)
            };
        }

        public GetUsersResponse GetUsers(GetUsersRequest request = null)
        {
            return new GetUsersResponse()
            {
                Users = _userRepository.GetAll()
            };
        }
    }
}
