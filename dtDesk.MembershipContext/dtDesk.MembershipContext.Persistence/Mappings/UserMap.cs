﻿using System.Data.Entity.ModelConfiguration;
using dtDesk.MembershipContext.DomainModel.Users;

namespace dtDesk.MembershipContext.Persistence.Mappings
{
    internal class UserMap : EntityTypeConfiguration<User>
    {
        internal UserMap()
        {
            // Primary Key
            HasKey(t => t.UserId);

            // Properties
            Property(t => t.FullName)
                .IsRequired()
                .HasColumnName("FullName");
            Property(t => t.Login)
                .IsRequired()
                .HasColumnName("Login");
            Property(t => t.Password)
                .IsRequired()
                .HasColumnName("Password");

            // Table and relationships 
            ToTable("Users");
            //HasMany(o => o.UserPermissions);
        }
    }
}
