﻿using System;
using System.Collections.Generic;
using System.Linq;
using dtDesk.MembershipContext.DomainModel.Repositories;
using dtDesk.MembershipContext.DomainModel.Users;

namespace dtDesk.MembershipContext.Persistence.Repositories
{
    public class UserRepository : IUserRepository
    {
        public IList<User> GetAll()
        {
            return Find(All);
        }

        private bool All(User user)
        {
            return true;
        }

        public bool Add(User user)
        {
            throw new NotImplementedException();
        }

        public bool Save(User user)
        {
            using (var db = new DomainModelDatabase())
            {
                try
                {
                    User userFromDb = db.Set<User>().Find(user.UserId);
                    if (userFromDb == null)
                    {
                        throw new Exception("Cannot find user from database. userId: " + user.UserId);
                    }

                    db.Entry(userFromDb).CurrentValues.SetValues(user);

                    return db.SaveChanges() > 0;
                }
                catch (InvalidOperationException ex)
                {
                    throw ex;
                }
            }
        }

        public bool Delete(User user)
        {
            throw new NotImplementedException();
        }

        public IList<User> Find(Func<User, bool> predicate)
        {
            using (var db = new DomainModelDatabase())
            {
                return db.Users.Where(predicate).ToList();
            }
        }
    }
}
