﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using dtDesk.MembershipContext.DomainModel.Users;

namespace dtDesk.MembershipContext.Persistence.Utils
{
    internal class SampleAppInitializer : DropCreateDatabaseAlways<DomainModelDatabase>
    {
        protected override void Seed(DomainModelDatabase context)
        {
            /////////////////////////////////////////////////////////////////
            // Users
            var users = new List<User>
            {
                new User(1, "Kuanysh Nabiyev", "kuanysh", "1"),
                new User(2, "Dastan Kabylbekov", "dastan", "1"),  
                new User(3, "Ivanov Ivan", "ivan", "1")
            };
            context.Users.AddRange(users);

            /////////////////////////////////////////////////////////////////
            // Permissions
            var defaultPerformer = users.FirstOrDefault(u => u.UserId == 1);
            var Operator = users.FirstOrDefault(u => u.UserId == 2);
            var client = users.FirstOrDefault(u => u.UserId == 3);

            // All standards will
            base.Seed(context);
        }
    }
}
