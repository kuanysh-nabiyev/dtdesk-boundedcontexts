using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using dtDesk.MembershipContext.DomainModel.Users;
using dtDesk.MembershipContext.Persistence.Mappings;
using dtDesk.MembershipContext.Persistence.Utils;

namespace dtDesk.MembershipContext.Persistence
{
    public class DomainModelDatabase : DbContext
    {
        static DomainModelDatabase()
        {
            // Fixed "Provider not loaded" error
            // http://stackoverflow.com/questions/21641435/error-no-entity-framework-provider-found-for-the-ado-net-provider-with-invarian
            var ensureDllIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;

            Database.SetInitializer(new SampleAppInitializer());
        }

        public DomainModelDatabase()
            : base("dtDeskDB.Membership")   // specify here conn-string entry if using SQL Server
        {
            Users = base.Set<User>();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            EnsureDatabase(modelBuilder);
        }

        public DbSet<User> Users { get; private set; }

        #region Database modeling
        public static void EnsureDatabase(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Configurations.Add(new UserMap());
        }
        #endregion
    }
}