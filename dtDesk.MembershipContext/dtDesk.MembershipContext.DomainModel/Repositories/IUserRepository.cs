﻿using System;
using System.Collections.Generic;
using dtDesk.MembershipContext.DomainModel.Users;

namespace dtDesk.MembershipContext.DomainModel.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
    }
}
