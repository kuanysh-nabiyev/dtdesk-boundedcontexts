﻿namespace dtDesk.MembershipContext.DomainModel.Users
{
    public enum Role
    {
        Administrator = 0,
        HeadOfAllDepartments = 1,
        HeadOfDepartment = 2,
        Employee = 3,
        Client = 4,
        Operator = 5
    }
}
