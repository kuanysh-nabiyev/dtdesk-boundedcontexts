﻿namespace dtDesk.MembershipContext.DomainModel.Users
{
    public enum Permission
    {
        CreateTicket,
        CloseTicket,
        ChangeClient,
        ChangeTicketPriority,
        AssignOrReassignTicketPerformer,
        ViewAllTickets,
        ViewDepartmentTickets
    }
}
