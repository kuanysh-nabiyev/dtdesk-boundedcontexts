﻿using System;
using System.Collections.Generic;

namespace dtDesk.MembershipContext.DomainModel.Users
{
    public class User : IAggregateRoot
    {
        // Added for convenience only -- to quickly populate the DB on app startup.
        public User(int id, String fullName, String login, String password)
        {
            UserId = id;
            FullName = fullName;
            Login = login;
            Password = password;
        }

        /// <summary>
        /// Used by the O/RM to materialize objects
        /// </summary>
        protected User()
        {
        }

        public int UserId { get; private set; }

        public String FullName { get; private set; }

        public String Login { get; private set; }

        public String Password { get; private set; }

        public Role Role { get; private set; }

        public bool IsActive { get; private set; }

        public ICollection<Permission> Permissions { get; private set; }

        public bool HasPermissionTo(Permission permission)
        {
            return this.Permissions.Contains(permission);
        }
    }
}
