﻿using dtDesk.MembershipContext.DomainModel.Users;

namespace dtDesk.MembershipContext.DomainModel.DomainServices
{
    public interface IUserService
    {
        User GetUserDetails(int userId);
    }
}