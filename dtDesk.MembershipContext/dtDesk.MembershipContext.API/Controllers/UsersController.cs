﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using dtDesk.MembershipContext.Application.Messaging;
using dtDesk.MembershipContext.Application.Messaging.Requests;
using dtDesk.MembershipContext.API.ViewModels;
using dtDesk.MembershipContext.Domain.Services;
using dtDesk.MembershipContext.DomainModel.DomainServices;
using dtDesk.MembershipContext.DomainModel.Users;

namespace dtDesk.MembershipContext.API.Controllers
{
    public class UsersController : ApiController
    {
        private readonly IUserApplicationService _userApplicationService;

        public UsersController(IUserApplicationService userApplicationService)
        {
            _userApplicationService = userApplicationService;
        }

        public UserViewModel GetUser(int id)
        {
            var response = _userApplicationService.GetUserDetails(
                new GetUserDetailsRequest()
                {
                    UserId = id
                });

            return new UserViewModel()
            {
                UserId = response.User.UserId,
                FullName = response.User.FullName
            };
        }

        public UsersViewModel GetAllUsers()
        {
            var response = _userApplicationService.GetUsers();

            var users = new List<UserViewModel>();

            foreach (var responseUser in response.Users)
            {
                users.Add(new UserViewModel()
                {
                    UserId = responseUser.UserId,
                    FullName = responseUser.FullName
                });
            }

            return new UsersViewModel()
            {
                Users = users 
            };
        }
    }
}
