﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Dependencies;
using dtDesk.MembershipContext.Application.Messaging;
using dtDesk.MembershipContext.DomainModel.DomainServices;
using dtDesk.MembershipContext.DomainModel.Repositories;
using Microsoft.Practices.Unity;

namespace dtDesk.MembershipContext.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
