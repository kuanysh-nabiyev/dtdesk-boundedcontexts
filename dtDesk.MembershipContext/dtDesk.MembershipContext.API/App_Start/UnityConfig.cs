using Microsoft.Practices.Unity;
using System.Web.Http;
using dtDesk.MembershipContext.Application.Messaging;
using dtDesk.MembershipContext.Application.Services;
using dtDesk.MembershipContext.Domain.Services;
using dtDesk.MembershipContext.DomainModel.DomainServices;
using dtDesk.MembershipContext.DomainModel.Repositories;
using dtDesk.MembershipContext.Persistence.Repositories;
using Unity.WebApi;

namespace dtDesk.MembershipContext.API
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            
            container.RegisterType<IUserRepository, UserRepository>();
            container.RegisterType<IUserService, UserService>();
            container.RegisterType<IUserApplicationService, UserApplicationService>();

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}