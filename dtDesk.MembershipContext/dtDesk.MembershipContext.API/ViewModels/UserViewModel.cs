using System;

namespace dtDesk.MembershipContext.API.ViewModels
{
    public class UserViewModel
    {
        public int UserId { get; set; }

        public String FullName { get; set; }
    }
}