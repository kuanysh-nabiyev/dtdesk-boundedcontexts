using System.Collections.Generic;

namespace dtDesk.MembershipContext.API.ViewModels
{
    public class UsersViewModel
    {
        public IEnumerable<UserViewModel> Users { get; set; }
    }
}