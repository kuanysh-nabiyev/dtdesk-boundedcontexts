﻿using dtDesk.TicketingBC.Application.Messaging;
using dtDesk.TicketingBC.Application.Messaging.Requests;
using dtDesk.TicketingBC.Application.Messaging.Responses;
using dtDesk.TicketingBC.CommandModel.DomainServices;
using dtDesk.TicketingBC.CommandModel.Services;
using dtDesk.TicketingBC.QueryModel.DomainServices;
using dtDesk.TicketingBC.QueryModel.Services;

namespace dtDesk.TicketingBC.Application.Services
{
    public class TicketApplicationService : ITicketApplicationService
    {
        private readonly IDbInitService _dbInitService;
        private readonly ICatalogService _catalogService;

        public TicketApplicationService(
            IDbInitService dbInitService, 
            ICatalogService catalogService)
        {
            _dbInitService = dbInitService;
            _catalogService = catalogService;
        }

        public GetDashboardInfoResponse GetDashboardInfo(GetDashboardInfoRequest getDashboardInfoRequest)
        {
            _dbInitService.InitDb();

            return new GetDashboardInfoResponse()
            {
                RecentTickets = _catalogService.GetRecentTickets(getDashboardInfoRequest.NumberOfRecentTickets),
                NumberOfTicketsAreBeingWorkedOnByCurrentUser = _catalogService
                    .GetNumberOfTicketsAreBeingWorkedOnByEmployee(getDashboardInfoRequest.CurrentUserId),
                NumberOfClosedTickets = _catalogService.GetNumberOfClosedTickets(),
                NumberOfOpenTickets = _catalogService.GetNumberOfOpenTickets()
            };
        }
    }
}
