﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using dtDesk.TicketingBC.QueryModel.DomainServices;
using dtDesk.TicketingBC.QueryModel.Persistence;
using dtDesk.TicketingBC.QueryModel.Tickets;

namespace dtDesk.TicketingBC.QueryModel.Services
{
    public class CatalogService : ICatalogService
    {
        private readonly IQueryModelDatabase _db;

        public CatalogService()
            : this(new QueryModelDatabase())
        {
            // TODO: delete after adding DI container and remove dtDesk.QueryModel.Persistence reference
        }

        public CatalogService(IQueryModelDatabase queryModelDatabase)
        {
            _db = queryModelDatabase;
        }

        public IReadOnlyCollection<Ticket> GetRecentTickets(int numberOfRecentTickets)
        {
            var tickets = _db.Tickets
                .OrderByDescending(t => t.DateOfCreation)
                .Take(numberOfRecentTickets)
                .ToList();

            return tickets;
        }

        public int GetNumberOfTicketsAreBeingWorkedOnByEmployee(int userId)
        {
            var numberOfTickets = _db.Tickets
                .Where(t => t.Status == TicketStatus.WorkInProgress)
                .Include(t => t.Assignments)
                .Count(t => t.Assignments.Any(a => a.Performer.UserId == userId));
                
            return numberOfTickets;
        }

        public int GetNumberOfClosedTickets()
        {
            // TODO
            return 0;
        }

        public int GetNumberOfOpenTickets()
        {
            // TODO
            return 0;
        }
    }
}