﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using dtDesk.TicketingBC.Application.Messaging;
using dtDesk.TicketingBC.Application.Messaging.Requests;
using dtDesk.TicketingBC.Application.Messaging.Responses;
using dtDesk.TicketingBC.Application.Services;
using dtDesk.TicketingBC.API.ViewModels;
using dtDesk.TicketingBC.QueryModel.Tickets;

namespace dtDesk.TicketingBC.API.Controllers
{
    public class TicketsController : ApiController
    {
        private readonly ITicketApplicationService _ticketApplicationService;

        public TicketsController(ITicketApplicationService ticketApplicationService)
        {
            _ticketApplicationService = ticketApplicationService;
        }

        [HttpGet]
        [ActionName("DashboardInfo")]
        public DashboardInfoViewModel GetDashboardInfo()
        {
            var request = new GetDashboardInfoRequest()
            {
                CurrentUserId = 1,
                NumberOfRecentTickets = 10
            };

            var response = _ticketApplicationService.GetDashboardInfo(request);

            var recentTickets = new List<TicketViewModel>();
            foreach (var ticket in response.RecentTickets)
            {
                recentTickets.Add(new TicketViewModel()
                {
                    Title = ticket.Title,
                    Description = ticket.Description
                });
            }

            return new DashboardInfoViewModel()
            {
                NumberOfClosedTickets = response.NumberOfClosedTickets,
                NumberOfTicketsAreBeingWorkedOnByCurrentUser = response.NumberOfTicketsAreBeingWorkedOnByCurrentUser,
                NumberOfOpenTickets = response.NumberOfOpenTickets,
                RecentTickets = recentTickets
            };
        }
        
    }
}
