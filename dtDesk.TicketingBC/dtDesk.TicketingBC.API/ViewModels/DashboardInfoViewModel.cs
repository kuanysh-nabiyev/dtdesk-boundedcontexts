﻿using System.Collections.Generic;

namespace dtDesk.TicketingBC.API.ViewModels
{
    public class DashboardInfoViewModel
    {
        public IEnumerable<TicketViewModel> RecentTickets { get; set; }
        public int NumberOfOpenTickets { get; set; }
        public int NumberOfTicketsAreBeingWorkedOnByCurrentUser { get; set; }
        public int NumberOfClosedTickets { get; set; }
    }
}