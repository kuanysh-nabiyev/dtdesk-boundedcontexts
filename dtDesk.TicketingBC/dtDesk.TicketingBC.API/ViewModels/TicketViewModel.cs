﻿using System;

namespace dtDesk.TicketingBC.API.ViewModels
{
    public class TicketViewModel
    {
        public String Title { get; set; }

        public String Description { get; set; }
    }
}