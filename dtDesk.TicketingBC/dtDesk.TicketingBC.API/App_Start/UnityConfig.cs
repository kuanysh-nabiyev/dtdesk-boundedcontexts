using Microsoft.Practices.Unity;
using System.Web.Http;
using dtDesk.TicketingBC.Application.Messaging;
using dtDesk.TicketingBC.Application.Services;
using dtDesk.TicketingBC.CommandModel.DomainServices;
using dtDesk.TicketingBC.CommandModel.Services;
using dtDesk.TicketingBC.QueryModel;
using dtDesk.TicketingBC.QueryModel.DomainServices;
using dtDesk.TicketingBC.QueryModel.Persistence;
using dtDesk.TicketingBC.QueryModel.Services;
using Unity.WebApi;

namespace dtDesk.TicketingBC.API
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            container.RegisterType<ITicketApplicationService, TicketApplicationService>();
            container.RegisterType<IDbInitService, DbInitService>();
            container.RegisterType<ICatalogService, CatalogService>();
            container.RegisterType<IQueryModelDatabase, QueryModelDatabase>();

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}