﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using dtDesk.TicketingBC.CommandModel.Persistence.Mappings;
using dtDesk.TicketingBC.CommandModel.Persistence.Utils;
using dtDesk.TicketingBC.CommandModel.Tickets;

namespace dtDesk.TicketingBC.CommandModel.Persistence
{
    public class CommandModelDatabase : DbContext
    {
        static CommandModelDatabase()
        {
            Database.SetInitializer(new SampleAppInitializer());
        }

        public CommandModelDatabase()
            : base("dtDeskDB.Ticketing")   // specify here conn-string entry if using SQL Server
        {
            Tickets = base.Set<Ticket>();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            EnsureDatabase(modelBuilder);
        }

        public DbSet<Ticket> Tickets { get; private set; }

        #region Database modeling
        public static void EnsureDatabase(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new TicketMap());
            modelBuilder.Configurations.Add(new TicketAssignmentMap());
            modelBuilder.Configurations.Add(new TicketCommentMap());
        }
        #endregion
    }
}