﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using dtDesk.TicketingBC.CommandModel.Tickets;

namespace dtDesk.TicketingBC.CommandModel.Persistence.Utils
{
    internal class SampleAppInitializer : DropCreateDatabaseAlways<CommandModelDatabase>
    {
        protected override void Seed(CommandModelDatabase context)
        {
            /////////////////////////////////////////////////////////////////
            // Users
            var defaultPerformer = new Employee(1, "Kuanysh Nabiyev");
            var Operator = new Operator(2, "Dastan Kabylbekov");
            var client = new Client(3, "Ivan Ivanov");

            /////////////////////////////////////////////////////////////////
            // Tickets
            var tickets = new List<Ticket>
            {
                new Ticket(1, client, Operator, "Компьютеры и переферия", "Сломался мой копьютер", TicketStatus.WorkInProgress),
                new Ticket(2, client, Operator, "Интернет и локальная сеть", "Почините сеть"),
                new Ticket(3, client, Operator, "Видеонаблюдение", "Камера не снимает несколько дней", TicketStatus.WorkInProgress),
                new Ticket(4, client, Operator, "Принтеры (обслуживание)", "Мой принтер сломан"),
                new Ticket(5, client, Operator, "Принтеры (обслуживание)", "Принтер на 2 этаже требует ремонта"),
                new Ticket(6, client, Operator, "Интернет и локальная сеть", "Пропал интернет. Срочно почините"),
                new Ticket(7, client, Operator, "Компьютеры и переферия", "Монитор не включается"),
                new Ticket(8, client, Operator, "Проект Эвридок", "Ошибка при импорте файла"),
                new Ticket(9, client, Operator, "Проект Эвридок", "Поиск документа выполняется около 5 минут"),
                new Ticket(10, client, Operator, "Проект EAM", "Ошибка при удалении файла в таблице контрагентов"),
                new Ticket(11, client, Operator, "Проект EAM", "Как расшифровывается слово EAM?"),
                new Ticket(12, client, Operator, "Проект HIIP", "Ошибка при отправке файла"),
                new Ticket(13, client, Operator, "Проект HIIP", "Ошибка при получении файла"),
                new Ticket(14, client, Operator, "Проект 1", "Ошибка в проекте 1"),
                new Ticket(15, client, Operator, "Проект 2", "Ошибка в проекте 2"),
                new Ticket(16, client, Operator, "Проект 3", "Ошибка в проекте 3"),
                new Ticket(17, client, Operator, "Проект 4", "Ошибка в проекте 4"),
                new Ticket(18, client, Operator, "Проект 5", "Ошибка в проекте 5"),
                new Ticket(19, client, Operator, "Проект 6", "Ошибка в проекте 6"),
                new Ticket(20, client, Operator, "Проект 7", "Ошибка в проекте 7"),
                new Ticket(21, client, Operator, "Проект 8", "Ошибка в проекте 8"),
                new Ticket(22, client, Operator, "Проект 9", "Ошибка в проекте 9"),
                new Ticket(23, client, Operator, "Проект 10", "Ошибка в проекте 10"),
                new Ticket(24, client, Operator, "Проект 11", "Ошибка в проекте 11"),
            };
            context.Tickets.AddRange(tickets);

            var ticket = tickets.FirstOrDefault(t => t.TicketId == 1);
            ticket.Assignments.Add(new TicketAssignment(10, Operator, defaultPerformer));

            ticket = tickets.FirstOrDefault(t => t.TicketId == 2);
            ticket.Assignments.Add(new TicketAssignment(20, Operator, defaultPerformer));

            ticket = tickets.FirstOrDefault(t => t.TicketId == 3);
            ticket.Assignments.Add(new TicketAssignment(30, Operator, defaultPerformer));

            ticket = tickets.FirstOrDefault(t => t.TicketId == 4);
            ticket.Assignments.Add(new TicketAssignment(40, Operator, defaultPerformer));

            ticket = tickets.FirstOrDefault(t => t.TicketId == 5);
            ticket.Assignments.Add(new TicketAssignment(50, Operator, defaultPerformer));

            ticket = tickets.FirstOrDefault(t => t.TicketId == 6);
            ticket.Assignments.Add(new TicketAssignment(60, Operator, defaultPerformer));

            ticket = tickets.FirstOrDefault(t => t.TicketId == 7);
            ticket.Assignments.Add(new TicketAssignment(70, Operator, defaultPerformer));

            ticket = tickets.FirstOrDefault(t => t.TicketId == 8);
            ticket.Assignments.Add(new TicketAssignment(80, Operator, defaultPerformer));

            ticket = tickets.FirstOrDefault(t => t.TicketId == 9);
            ticket.Assignments.Add(new TicketAssignment(90, Operator, defaultPerformer));

            ticket = tickets.FirstOrDefault(t => t.TicketId == 10);
            ticket.Assignments.Add(new TicketAssignment(100, Operator, defaultPerformer));

            ticket = tickets.FirstOrDefault(t => t.TicketId == 11);
            ticket.Assignments.Add(new TicketAssignment(110, Operator, defaultPerformer));

            ticket = tickets.FirstOrDefault(t => t.TicketId == 12);
            ticket.Assignments.Add(new TicketAssignment(120, Operator, defaultPerformer));

            ticket = tickets.FirstOrDefault(t => t.TicketId == 13);
            ticket.Assignments.Add(new TicketAssignment(130, Operator, defaultPerformer));

            ticket = tickets.FirstOrDefault(t => t.TicketId == 14);
            ticket.Assignments.Add(new TicketAssignment(140, Operator, defaultPerformer));

            ticket = tickets.FirstOrDefault(t => t.TicketId == 15);
            ticket.Assignments.Add(new TicketAssignment(150, Operator, defaultPerformer));

            ticket = tickets.FirstOrDefault(t => t.TicketId == 16);
            ticket.Assignments.Add(new TicketAssignment(160, Operator, defaultPerformer));

            ticket = tickets.FirstOrDefault(t => t.TicketId == 17);
            ticket.Assignments.Add(new TicketAssignment(170, Operator, defaultPerformer));

            ticket = tickets.FirstOrDefault(t => t.TicketId == 18);
            ticket.Assignments.Add(new TicketAssignment(180, Operator, defaultPerformer));

            ticket = tickets.FirstOrDefault(t => t.TicketId == 19);
            ticket.Assignments.Add(new TicketAssignment(190, Operator, defaultPerformer));

            ticket = tickets.FirstOrDefault(t => t.TicketId == 20);
            ticket.Assignments.Add(new TicketAssignment(200, Operator, defaultPerformer));

            ticket = tickets.FirstOrDefault(t => t.TicketId == 21);
            ticket.Assignments.Add(new TicketAssignment(210, Operator, defaultPerformer));

            ticket = tickets.FirstOrDefault(t => t.TicketId == 22);
            ticket.Assignments.Add(new TicketAssignment(220, Operator, defaultPerformer));

            ticket = tickets.FirstOrDefault(t => t.TicketId == 23);
            ticket.Assignments.Add(new TicketAssignment(230, Operator, defaultPerformer));

            ticket = tickets.FirstOrDefault(t => t.TicketId == 24);
            ticket.Assignments.Add(new TicketAssignment(240, Operator, defaultPerformer));

            // All standards will
            base.Seed(context);
        }
    }
}
