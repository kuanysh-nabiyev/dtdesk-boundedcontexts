﻿using System.Data.Entity.ModelConfiguration;
using dtDesk.TicketingBC.CommandModel.Tickets;

namespace dtDesk.TicketingBC.CommandModel.Persistence.Mappings
{
    internal class UserMap : EntityTypeConfiguration<User>
    {
        internal UserMap()
        {
            // Primary Key
            HasKey(t => t.UserId);

            // Properties
            Property(t => t.FullName)
                .IsRequired()
                .HasColumnName("FullName");

            // Table and relationships 
            ToTable("Users");
            //HasMany(o => o.UserPermissions);
        }
    }
}
