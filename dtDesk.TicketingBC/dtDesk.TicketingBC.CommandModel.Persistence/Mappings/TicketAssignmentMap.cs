﻿using System.Data.Entity.ModelConfiguration;
using dtDesk.TicketingBC.CommandModel.Tickets;

namespace dtDesk.TicketingBC.CommandModel.Persistence.Mappings
{
    internal class TicketAssignmentMap : EntityTypeConfiguration<TicketAssignment>
    {
        internal TicketAssignmentMap()
        {
            // Primary Key
            HasKey(t => t.AssignmentId);

            // Table and relationships 
            ToTable("TicketAssignments");
        }
    }
}