﻿using System.Data.Entity.ModelConfiguration;
using dtDesk.TicketingBC.CommandModel.Tickets;

namespace dtDesk.TicketingBC.CommandModel.Persistence.Mappings
{
    internal class TicketCommentMap : EntityTypeConfiguration<TicketComment>
    {
        internal TicketCommentMap()
        {
            // Primary Key
            HasKey(t => t.CommentId);

            // Table and relationships 
            ToTable("TicketComments");
        }
    }
}