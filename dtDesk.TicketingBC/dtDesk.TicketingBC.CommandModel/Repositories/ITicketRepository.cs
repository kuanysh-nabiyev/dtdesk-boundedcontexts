﻿using dtDesk.TicketingBC.CommandModel.Tickets;

namespace dtDesk.TicketingBC.CommandModel.Repositories
{
    public interface ITicketRepository : IRepository<Ticket>
    {
    }
}
