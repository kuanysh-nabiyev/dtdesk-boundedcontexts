﻿using System.Collections.Generic;

namespace dtDesk.TicketingBC.CommandModel.Repositories
{
    public interface IRepository<TAggregate>
    {
        /// <summary>
        /// Add an aggregate graph to the store.
        /// </summary>
        /// <param name="aggregate">Aggregate root object</param>
        /// <returns>True if successful; False otherwise</returns>
        bool Add(TAggregate aggregate);

        /// <summary>
        /// Save changes to an aggregate graph already in the store.
        /// </summary>
        /// <param name="aggregate">Aggregate root object</param>
        /// <returns>True if successful; False otherwise</returns>
        bool Save(TAggregate aggregate);

        /// <summary>
        /// Delete the entire graph rooted in the specified aggregate object. 
        /// Cascading rules must be set at the DB level.
        /// </summary>
        /// <param name="aggregate">Aggregate root object</param>
        /// <returns>True if successful; False otherwise</returns>
        bool Delete(TAggregate aggregate);
    }
}
