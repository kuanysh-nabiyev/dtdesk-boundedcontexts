﻿namespace dtDesk.TicketingBC.CommandModel.Tickets
{
    public class SecondLineEmployee : Employee
    {
        public SecondLineEmployee(int id, string fullName) 
            : base(id, fullName)
        {
        }
    }
}