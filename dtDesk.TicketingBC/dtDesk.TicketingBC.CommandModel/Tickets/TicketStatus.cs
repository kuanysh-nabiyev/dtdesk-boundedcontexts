﻿namespace dtDesk.TicketingBC.CommandModel.Tickets
{
    public enum TicketStatus
    {
        Open = 0,
        WorkInProgress = 1,
        Closed = 2
    }
}
