﻿using System;

namespace dtDesk.TicketingBC.CommandModel.Tickets
{
    public class TicketComment
    {
        public int CommentId { get; private set; }

        public String Text { get; private set; }

        public User WrittenBy { get; private set; }

        public DateTime DateOfWriting { get; private set; }
    }
}
