﻿using System;

namespace dtDesk.TicketingBC.CommandModel.Tickets
{
    public class User : IAggregateRoot
    {
        // Added for convenience only -- to quickly populate the DB on app startup.
        public User(int id, String fullName)
        {
            UserId = id;
            FullName = fullName;
        }

        public int UserId { get; private set; }

        public String FullName { get; private set; }
    }
}