﻿using System;

namespace dtDesk.TicketingBC.CommandModel.Tickets
{
    public class TicketAssignment
    {
        // Added for convenience only -- to quickly populate the DB on app startup.
        public TicketAssignment(int id, Employee assignedBy, Employee performer)
        {
            AssignmentId = id;
            AssignedBy = assignedBy;
            DateOfAssign = DateTime.UtcNow;
            Performer = performer;
        }

        public int AssignmentId { get; private set; }

        public Employee AssignedBy { get; private set; }

        public DateTime DateOfAssign { get; private set; }

        public Employee Performer { get; private set; }
    }
}
