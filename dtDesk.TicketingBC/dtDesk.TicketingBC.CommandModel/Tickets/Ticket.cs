﻿using System;
using System.Collections.Generic;

namespace dtDesk.TicketingBC.CommandModel.Tickets
{
    public class Ticket : IAggregateRoot
    {
        // Added for convenience only -- to quickly populate the DB on app startup.
        public Ticket(int id, Client fromWhom, Operator createdBy, String title, string description, 
            TicketStatus status = TicketStatus.Open)
        {
            TicketId = id;
            Description = description;
            FromWhom = fromWhom;
            CreatedBy = createdBy;
            Title = title;
            DateOfCreation = DateTime.UtcNow;
            Priority = TicketPriority.Medium;
            Status = status;
            Assignments = new List<TicketAssignment>();
            Comments = new List<TicketComment>();
        }

        #region Added to please the O/RM

        /// <summary>
        /// Used by the O/RM to materialize objects
        /// </summary>
        protected Ticket()
        {
        }

        #endregion

        public int TicketId { get; private set; }

        public Client FromWhom { get; private set; }

        public Operator CreatedBy { get; private set; }

        public TicketPriority Priority { get; private set; }

        public String Title { get; private set; }

        public String Description { get; private set; }

        public TicketStatus Status { get; private set; }

        public DateTime DateOfCreation { get; private set; }

        public String UploadedFileName { get; private set; }

        public ICollection<TicketComment> Comments { get; private set; }

        public ICollection<TicketAssignment> Assignments { get; private set; }
    }
}
