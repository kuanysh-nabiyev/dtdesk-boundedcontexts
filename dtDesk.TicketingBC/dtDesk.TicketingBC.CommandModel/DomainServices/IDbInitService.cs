﻿namespace dtDesk.TicketingBC.CommandModel.DomainServices
{
    public interface IDbInitService
    {
        void InitDb();
    }
}