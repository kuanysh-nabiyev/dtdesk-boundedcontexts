﻿using System.Linq;
using dtDesk.TicketingBC.CommandModel.DomainServices;
using dtDesk.TicketingBC.CommandModel.Persistence;

namespace dtDesk.TicketingBC.CommandModel.Services
{
    public class DbInitService : IDbInitService
    {
        public void InitDb()
        {
            using (var db = new CommandModelDatabase())
            {
                // for calling SampleAppInitializer to seed data
                var tickets = db.Tickets.Take(3).ToList();
            }
        }
    }
}