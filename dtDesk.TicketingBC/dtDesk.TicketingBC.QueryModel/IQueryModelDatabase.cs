﻿using System.Linq;
using dtDesk.TicketingBC.QueryModel.Tickets;

namespace dtDesk.TicketingBC.QueryModel
{
    public interface IQueryModelDatabase
    {
        IQueryable<Ticket> Tickets { get; }
    }
}