using System.Collections.Generic;
using dtDesk.TicketingBC.QueryModel.Tickets;

namespace dtDesk.TicketingBC.QueryModel.DomainServices
{
    public interface ICatalogService
    {
        IReadOnlyCollection<Ticket> GetRecentTickets(int numberOfRecentTickets);

        int GetNumberOfTicketsAreBeingWorkedOnByEmployee(int userId);
        int GetNumberOfClosedTickets();
        int GetNumberOfOpenTickets();
    }
}