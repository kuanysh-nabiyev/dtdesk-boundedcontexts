﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using dtDesk.TicketingBC.QueryModel.Users;
using System.Linq.Expressions;
using System.Runtime.Remoting.Contexts;

namespace dtDesk.TicketingBC.QueryModel.Tickets
{
    public class Ticket
    {
        #region Added to please the O/RM

        /// <summary>
        /// Used by the O/RM to materialize objects
        /// </summary>
        //protected Ticket()
        //{
        //}

        #endregion

        public int TicketId { get; private set; }

        public Client FromWhom { get; private set; }

        public Operator CreatedBy { get; private set; }

        public TicketPriority Priority { get; private set; }

        public String Title { get; private set; }

        public String Description { get; private set; }

        public TicketStatus Status { get; private set; }

        public DateTime DateOfCreation { get; private set; }

        public String UploadedFileName { get; private set; }

        public IReadOnlyCollection<TicketComment> Comments { get; private set; }

        //protected virtual ICollection<TicketAssignment> AssignmentsStorage { get; set; }

        //public IEnumerable<TicketAssignment> Assignments
        //{
        //    get { return AssignmentsStorage; }

        //}

        //public static Expression<Func<Ticket, ICollection<TicketAssignment>>> AssignmentsAccessor = x => x.AssignmentsStorage;

        public ICollection<TicketAssignment> Assignments { get; private set; }
    }
}
