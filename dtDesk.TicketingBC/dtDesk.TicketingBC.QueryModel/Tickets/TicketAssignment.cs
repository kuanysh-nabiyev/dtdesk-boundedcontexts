﻿using System;
using dtDesk.TicketingBC.QueryModel.Users;

namespace dtDesk.TicketingBC.QueryModel.Tickets
{
    public class TicketAssignment
    {
        public int AssignmentId { get; private set; }

        public Employee AssignedBy { get; private set; }

        public DateTime DateOfAssign { get; private set; }

        public SecondLineEmployee Performer { get; private set; }
    }
}
