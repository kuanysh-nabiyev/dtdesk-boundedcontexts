﻿using System;
using dtDesk.TicketingBC.QueryModel.Users;

namespace dtDesk.TicketingBC.QueryModel.Tickets
{
    public class TicketComment
    {
        public int CommentId { get; private set; }

        public String Text { get; private set; }

        public User WrittenBy { get; private set; }

        public DateTime DateOfWriting { get; private set; }
    }
}
