﻿namespace dtDesk.TicketingBC.QueryModel.Tickets
{
    public enum TicketStatus
    {
        Open = 0,
        WorkInProgress = 1,
        Closed = 1
    }
}
