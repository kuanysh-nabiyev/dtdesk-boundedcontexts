﻿using System;

namespace dtDesk.TicketingBC.QueryModel.Users
{
    public class User
    {
        public int UserId { get; private set; }

        public String FullName { get; private set; }
    }
}