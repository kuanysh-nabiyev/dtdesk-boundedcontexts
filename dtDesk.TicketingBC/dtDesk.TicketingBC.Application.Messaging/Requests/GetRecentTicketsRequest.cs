namespace dtDesk.TicketingBC.Application.Messaging.Requests
{
    public class GetRecentTicketsRequest
    {
        private int _numberOfRecentTickets;
        public int NumberOfRecentTickets
        {
            get
            {
                if (_numberOfRecentTickets < 1)
                    return 5;

                return _numberOfRecentTickets;
            }
            set { _numberOfRecentTickets = value; }
        }
    }
}