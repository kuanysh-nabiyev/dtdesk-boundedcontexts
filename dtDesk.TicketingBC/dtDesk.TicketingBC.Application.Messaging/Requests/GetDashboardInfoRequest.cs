﻿namespace dtDesk.TicketingBC.Application.Messaging.Requests
{
    public class GetDashboardInfoRequest
    {
        public int CurrentUserId { get; set; }

        private int _numberOfRecentTickets;
        public int NumberOfRecentTickets
        {
            get
            {
                if (_numberOfRecentTickets < 1)
                    return 5;

                return _numberOfRecentTickets;
            }
            set { _numberOfRecentTickets = value; }
        }
    }
}