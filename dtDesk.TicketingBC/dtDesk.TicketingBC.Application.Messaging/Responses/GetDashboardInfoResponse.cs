﻿using System.Collections.Generic;
using dtDesk.TicketingBC.QueryModel.Tickets;

namespace dtDesk.TicketingBC.Application.Messaging.Responses
{
    public class GetDashboardInfoResponse : ResponseBase
    {
        public IReadOnlyCollection<Ticket> RecentTickets { get; set; }
        public int NumberOfOpenTickets { get; set; }
        public int NumberOfTicketsAreBeingWorkedOnByCurrentUser { get; set; }
        public int NumberOfClosedTickets { get; set; }
    }
}
