﻿using System.Collections.Generic;
using dtDesk.TicketingBC.QueryModel.Tickets;

namespace dtDesk.TicketingBC.Application.Messaging.Responses
{
    public class GetRecentTicketsResponse
    {
        public IReadOnlyCollection<Ticket> RecentTickets { get; set; }
    }
}