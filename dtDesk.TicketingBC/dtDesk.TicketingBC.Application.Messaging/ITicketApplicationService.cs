﻿using dtDesk.TicketingBC.Application.Messaging.Requests;
using dtDesk.TicketingBC.Application.Messaging.Responses;

namespace dtDesk.TicketingBC.Application.Messaging
{
    public interface ITicketApplicationService
    {
        GetDashboardInfoResponse GetDashboardInfo(GetDashboardInfoRequest getDashboardInfoRequest);
    }
}
