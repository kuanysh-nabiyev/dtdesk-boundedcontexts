﻿using System;
using System.Data.Entity;
using System.Linq;
using dtDesk.TicketingBC.QueryModel.Tickets;

namespace dtDesk.TicketingBC.QueryModel.Persistence
{
    public class QueryModelDatabase : IQueryModelDatabase, IDisposable
    {
        private readonly QueryDbContext _context;

        public QueryModelDatabase()
        {
            _context = new QueryDbContext("dtDeskDB.Ticketing");
            _tickets = _context.Set<Ticket>();
        }

        private readonly DbSet<Ticket> _tickets;

        public IQueryable<Ticket> Tickets
        {
            get { return _tickets; }
        }

        public void Dispose()
        {
            if (_context != null)
            {
                _context.Dispose();
            }
        }
    }
}
