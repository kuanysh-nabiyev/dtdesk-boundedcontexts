﻿using System.Data.Entity.ModelConfiguration;
using dtDesk.TicketingBC.QueryModel.Tickets;

namespace dtDesk.TicketingBC.QueryModel.Persistence.Mappings
{
    internal class TicketCommentMap : EntityTypeConfiguration<TicketComment>
    {
        internal TicketCommentMap()
        {
            // Primary Key
            HasKey(t => t.CommentId);

            // Table and relationships 
            ToTable("TicketComments");
        }
    }
}