﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using dtDesk.TicketingBC.QueryModel.Tickets;

namespace dtDesk.TicketingBC.QueryModel.Persistence.Mappings
{
    internal class TicketMap : EntityTypeConfiguration<Ticket>
    {
        internal TicketMap()
        {
            // Primary Key
            HasKey(t => t.TicketId);

            // Properties
            Property(t => t.Title)
                .IsRequired()
                .HasColumnName("Title");
            Property(t => t.Description)
                .IsRequired()
                .HasColumnName("Description");

            // Table and relationships 
            ToTable("Tickets");

            HasRequired(o => o.FromWhom);
            //HasMany(Ticket.AssignmentsAccessor);
            HasMany(t => t.Assignments);
        }
    }
}
