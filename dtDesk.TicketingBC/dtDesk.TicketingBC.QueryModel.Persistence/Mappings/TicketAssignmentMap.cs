﻿using System.Data.Entity.ModelConfiguration;
using dtDesk.TicketingBC.QueryModel.Tickets;

namespace dtDesk.TicketingBC.QueryModel.Persistence.Mappings
{
    internal class TicketAssignmentMap : EntityTypeConfiguration<TicketAssignment>
    {
        internal TicketAssignmentMap()
        {
            // Primary Key
            HasKey(t => t.AssignmentId);

            // Table and relationships 
            ToTable("TicketAssignments");
        }
    }
}