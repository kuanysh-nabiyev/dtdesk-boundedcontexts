﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using dtDesk.TicketingBC.QueryModel.Persistence.Mappings;

namespace dtDesk.TicketingBC.QueryModel.Persistence
{
    internal class QueryDbContext : DbContext
    {
        public QueryDbContext(String nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new TicketMap());
            modelBuilder.Configurations.Add(new TicketAssignmentMap());
            modelBuilder.Configurations.Add(new TicketCommentMap());
        }
    }
}