﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace dtDesk.CompositeUI.Web.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public async Task<ActionResult> Index()
        {
            using (var client = new MembershipContextHttpClient())
            {
                // New code:
                HttpResponseMessage response = await client.GetAsync("api/users/1");
                if (response.IsSuccessStatusCode)
                {
                    UserViewModel userViewModel = await response.Content.ReadAsAsync<UserViewModel>();

                    return View(userViewModel);
                }
            }

            return View();
        }

        public async Task<ActionResult> Dashboard()
        {
            using (var client = new TicketingContextHttpClient())
            {
                HttpResponseMessage response = await client.GetAsync("api/tickets/dashboardinfo");
                if (response.IsSuccessStatusCode)
                {
                    DashboardInfoViewModel dashboardInfoViewModel = await response.Content.ReadAsAsync<DashboardInfoViewModel>();

                    return View(dashboardInfoViewModel);
                }
            }

            return View();
        }
    }

    public class DashboardInfoViewModel
    {
        public IEnumerable<TicketViewModel> RecentTickets { get; set; }
        public int NumberOfOpenTickets { get; set; }
        public int NumberOfTicketsAreBeingWorkedOnByCurrentUser { get; set; }
        public int NumberOfClosedTickets { get; set; }
    }

    public class TicketViewModel
    {
        public String Title { get; set; }

        public String Description { get; set; }
    }

    public class MembershipContextHttpClient : HttpClient
    {
        public MembershipContextHttpClient()
        {
            this.BaseAddress = new Uri("http://localhost:42334/");
            this.DefaultRequestHeaders.Accept.Clear();
            this.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
    }

    public class TicketingContextHttpClient : HttpClient
    {
        public TicketingContextHttpClient()
        {
            this.BaseAddress = new Uri("http://localhost:1142/");
            this.DefaultRequestHeaders.Accept.Clear();
            this.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
    }

    public class UserViewModel
    {
        public int UserId { get; set; }

        public String FullName { get; set; }
    }
}